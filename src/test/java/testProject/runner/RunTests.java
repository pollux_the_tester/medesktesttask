package testProject.runner;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import testProject.suites.MedeskReceptionTestSuite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        MedeskReceptionTestSuite.class
})

public class RunTests {

}
