package testProject.utils;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.*;

public class BasicUtils {

    @Step("Open url: {url}")
    public static void openUrl(String url) {
        open(url);
    }

    @Step("Click button with text: {buttonText}")
    public static void clickButtonByText(String buttonText) {
        $(By.xpath(String.format("//button//*[text() = '%s']/..", buttonText))).click();
    }

    @Step("Click button which located in section with header: {sectionHeaderText}")
    public static void clickButtonBySectionHeaderText(String sectionHeaderText) {
        $(By.xpath(String.format("//*[text() = '%s']/../..//button", sectionHeaderText))).click();
    }

    @Step("Click button by name: {buttonName}")
    public static void clickButtonByName(String buttonName) {
        $(By.name("save")).click();
    }

    @Step("Scroll to selende element")
    public static void scrollToElement(SelenideElement element) {
        element.scrollTo();
    }

    @Step("Fill input field with placeholder: {placeholder} with text data: {value}")
    public static void fillInputByPlaceholder(String placeholder, String value) {
        $(By.xpath(String.format("//*[@placeholder = '%s']", placeholder))).val(value);
    }
}
