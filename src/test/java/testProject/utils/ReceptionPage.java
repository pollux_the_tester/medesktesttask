package testProject.utils;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static testProject.utils.BasicUtils.fillInputByPlaceholder;

public class ReceptionPage {

    @Step("Choose specialization: {specName}")
    public static void chooseSpecialization(String specName){
        $(By.xpath(String.format("//*[@class='page-card-content']//*[text() = 'Запись по специальности']/../..//*[text() = '%s']", specName))).click();
    }

    @Step("Choose specialist: {specialist}")
    public static void chooseSpecialist(String specialist){
        $(By.xpath(String.format("//*[text() = 'Выбор специалиста']/../../..//*[text() = '%s']", specialist))).click();
    }

    @Step("Get free time slots for chosen day")
    public static List<String> getFreeTimeslots(){
        List<String> timeslots = new ArrayList<String>();
        ElementsCollection elCol = $$(By.xpath("//*[@class = 'book-slot-select-slot']"));
        for (SelenideElement el : elCol){
            timeslots.add(el.getText());
        }
        return timeslots;
    }

    @Step("Get free time slots for chosen day")
    public static ElementsCollection getFreeTimeslotsElements(){
        ElementsCollection elCol = $$(By.xpath("//*[@class = 'book-slot-select-slot']"));
        return elCol;
    }

    @Step("Get Available booking days")
    public static ElementsCollection getAvailableForBookingDays(){
        ElementsCollection elCol = $$(By.xpath("//*[@class = 'md-datepicker-inline-day']"));
        return elCol;
    }

    @Step("Get active day for booking")
    public static ElementsCollection getActiveDayForBooking(){
        ElementsCollection elCol = $$(By.xpath("//*[@class = 'md-datepicker-inline-day md-datepicker-inline-day_active']"));
        return elCol;
    }

    @Step("Fill patients data form")
    public static void fillPatientsData(){
        fillInputByPlaceholder("Фамилия", "Test");
        fillInputByPlaceholder("Имя", "Test");
        fillInputByPlaceholder("Почта", "test@test.test");
        fillInputByPlaceholder("Номер мобильного телефона", "9000000000");
    }
}
