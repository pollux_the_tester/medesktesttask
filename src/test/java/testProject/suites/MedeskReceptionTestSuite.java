package testProject.suites;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Description;
import io.qameta.allure.junit4.DisplayName;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static com.codeborne.selenide.Selenide.sleep;
import static testProject.data.Vars.URL;
import static testProject.utils.BasicUtils.*;
import static testProject.utils.ReceptionPage.*;

public class MedeskReceptionTestSuite {

    @Before
    public void setUp() {
        Configuration.timeout = 10000;
        Configuration.browserSize = "1920x1080";
    }


    @Test
    @DisplayName("User book specialist time")
    @Description("User choose specialization")
    public void receiptionP1(){
        openUrl(URL);
        chooseSpecialization("Хирург");
        chooseSpecialist("Doctor A");
        clickButtonBySectionHeaderText("Врач");
        getAvailableForBookingDays().get(0).click();
        getFreeTimeslotsElements().get(0).click();
        clickButtonBySectionHeaderText("Выберите дату и время");
        fillPatientsData();
        clickButtonBySectionHeaderText("Ваши данные");
        sleep(10000);
    }
}
